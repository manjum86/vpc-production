#!/bin/bash
sudo apt update -y
sudo apt install apache2
sudo systemctl start apache2
usermod -a -G apache ec2-user
chown -R ec2-user:apache /var/www
chmod 2775 /var/www
echo "The page was created by the user data" | sudo tee /var/www/html/index.html
