variable "vpc_cidr" {
        default="10.0.0.0/19"
}

variable "public_subnet_cidrs" {
 type        = list(string)
 description = "Public Subnet CIDR values"
 default     = ["10.0.0.0/21", "10.0.8.0/21"]
}

variable "private_subnet_cidrs" {
 type        = list(string)
 description = "Private Subnet CIDR values"
 default     = ["10.0.16.0/21", "10.0.24.0/21"]
}

variable "azs" {
type = list(string)
description= "Availability Zones"
default =["us-east-1a","us-east-1b"]
}

variable "amiid" {
default="ami-06aa3f7caf3a30282"
}

variable "type" {
default="t2.micro"
}

variable "pemfile" {
default="AWSkeypair"
}

variable "user_data" {
  description = "boostrap script for apache"
  type        = string
  default     = <<EOF
  #!/bin/bash

sudo apt update -y
sudo apt install -y apache2
sudo systemctl start apache2

EOF
}
