provider "aws" {
        region="us-east-1"
}

resource "aws_vpc" "my_vpc" {
        cidr_block=var.vpc_cidr
        enable_dns_hostnames=true

tags= {
        Name="Demo_vpc"
}
}

resource "aws_subnet" "public_subnets" {
        vpc_id=aws_vpc.my_vpc.id
        count=length(var.public_subnet_cidrs)
        cidr_block =element(var.public_subnet_cidrs,count.index)
        availability_zone=element(var.azs,count.index)

tags = {
        Name="Demo_vpc_public_subnet ${count.index+1}-${var.azs[count.index]}"
}
}

resource "aws_subnet" "private_subnets" {
        vpc_id=aws_vpc.my_vpc.id
        count=length(var.private_subnet_cidrs)
        cidr_block =element(var.private_subnet_cidrs,count.index)
        availability_zone=element(var.azs,count.index)
tags = {
        Name="Demo_vpc_private Subnet ${count.index+1}-${var.azs[count.index]}"
}
}

resource "aws_internet_gateway" "my_vpc_igw" {
        vpc_id=aws_vpc.my_vpc.id
tags={
        name="internet gateway"
}
}

resource "aws_route_table" "public_route" {
 vpc_id = aws_vpc.my_vpc.id

 route {
   cidr_block = "0.0.0.0/0"
   gateway_id = aws_internet_gateway.my_vpc_igw.id
 }

 tags = {
   Name = "Demo_vpc_public_route"
 }
}

resource "aws_route_table_association" "public_subnet_asso" {
 count = length(var.public_subnet_cidrs)
 subnet_id      = element(aws_subnet.public_subnets[*].id, count.index)
 route_table_id = aws_route_table.public_route.id
}

resource "aws_eip" "eip_natgw" {
        count="2"
}

resource "aws_nat_gateway" "nat" {
        count = "2"
        allocation_id=aws_eip.eip_natgw[count.index].id
        subnet_id=element(aws_subnet.public_subnets[*].id, count.index)
tags={
        Name="Demo_vpc_public_nat ${count.index+1}-${var.azs[count.index]}"
}
}

resource "aws_route_table" "private_route" {
        count=length(var.private_subnet_cidrs)
        vpc_id = aws_vpc.my_vpc.id
route {
        cidr_block="0.0.0.0/0"
        gateway_id =aws_nat_gateway.nat[count.index].id
}
 tags = {
   Name = "Demo_vpc_route ${count.index+1}-${var.azs[count.index]}"
}
}

resource "aws_route_table_association" "private_subnet_asso" {
        count = length(var.private_subnet_cidrs)
        subnet_id=element(aws_subnet.private_subnets[*].id, count.index)
        route_table_id=aws_route_table.private_route[count.index].id

}

resource "aws_security_group" "autoscaling_secgrp" {
        name="autoscaling-security-group"
        vpc_id=aws_vpc.my_vpc.id

egress  {
        from_port=0
        to_port=0
        protocol= "-1"
        cidr_blocks=["0.0.0.0/0"]
}

ingress {
        from_port=22
        to_port=22
        protocol="tcp"
        cidr_blocks=["0.0.0.0/0"]
}

ingress {
        from_port=80
        to_port=80
        protocol="tcp"
        security_groups=[aws_security_group.alb_security_group.id]
}
}

resource "aws_launch_template" "my_launch_template" {
        name="my_launch_template"
        description=" to create autoscaling group"
        image_id=var.amiid
        instance_type=var.type
        key_name=var.pemfile
        vpc_security_group_ids=[aws_security_group.autoscaling_secgrp.id]

lifecycle {
        create_before_destroy=true
}
tags    = {
                Name="Demo_vpc_launch_template"
}
}

resource "aws_autoscaling_group" "demo_project_asg" {
        name_prefix="myasg_"
        vpc_zone_identifier=aws_subnet.private_subnets[*].id
        launch_template {
    id      = aws_launch_template.my_launch_template.id
    version = aws_launch_template.my_launch_template.latest_version
  }
        health_check_type = "EC2"
        health_check_grace_period = 300
        desired_capacity   = 2
        max_size           = 4
        min_size           = 2
        target_group_arns=[aws_lb_target_group.lb_target_group.arn]
 tag {
    key                 = "Name"
    value               = "Autoscaling project"
    propagate_at_launch = true
  }
}

resource "aws_security_group" "alb_security_group" {
  name        = "alb-security-group"
  description = "ALB Security Group"
  vpc_id      = aws_vpc.my_vpc.id

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
        from_port =0
        to_port=0
        protocol = "-1"
        cidr_blocks=["0.0.0.0/0"]
}
}

resource "aws_lb" "alb" {
         name               = "public-alb"
         internal           = false
         load_balancer_type = "application"
         security_groups    = [aws_security_group.alb_security_group.id]
         subnets            = aws_subnet.public_subnets[*].id
}

resource "aws_lb_target_group" "lb_target_group" {
        port= 80
        protocol="HTTP"
        vpc_id=aws_vpc.my_vpc.id
}

resource "aws_lb_listener" "alb_listener" {
        load_balancer_arn = aws_lb.alb.arn
        port = "80"
        protocol= "HTTP"

        default_action {
        type = "forward"
        target_group_arn=aws_lb_target_group.lb_target_group.arn
}
}

